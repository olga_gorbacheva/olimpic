package ru.god.task2;

/**
 * Класс, позволяющий создавать объекты типа "точка"
 *
 * @author Горбачева О. Д. 16ИТ18к
 */
class Point {
    /*
    Координата по оси Х
     */
    private int x;
    /*
    Координата по оси У
     */
    private int y;

    /**
     * Конструктор для создания объекта "точка"
     *
     * @param x - координата по оси Х
     * @param y - координата по оси У
     */
    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Метод, позволяющий определить, к какой четверти принадлежит точка
     *
     * @return номер четверти
     */
    int quarter() {
        if (x > 0) {
            if (y > 0) {
                return 1;
            }
            if (y < 0) {
                return 4;
            }
        }
        if (x < 0) {
            if (y > 0) {
                return 2;
            }
            if (y < 0) {
                return 3;
            }
        }
        return 0;
    }

    /**
     * Метод, позволяющий определить расстояние от точки до ближайшей оси
     *
     * @return расстояние от точки до ближайшей оси
     */
    int lenghtToAsix() {
        return Math.min(Math.abs(x), Math.abs(y));
    }

    @Override
    public String toString() {
        return "(x = " + x + ", y = " + y + ')';
    }
}

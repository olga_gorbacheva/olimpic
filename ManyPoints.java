package ru.god.task2;

import java.util.Scanner;

/**
 * Класс, реализующий работу с координатной системой
 * <p>
 * Ниже представлена программа, осуществляющая последовательный ввод нескольких точек с координатами, поиск по всей
 * координатной плоскости четверти с наибольшим количеством точек, поиск точки, наименее удаленной от осей координат и
 * расстояния от нее до ближайшей оси
 * <p>
 * Для работы с точками используется класс Point, полями которого являются координаты по Х и по У, соответственно
 * Более подробное описание точки, как объекта отдельного класса, содержится в комментарии к нему
 * <p>
 * При вводе строки, которая сождержит 2 координаты, используется метод split для вычленения самих координат из строки
 * Затем они уже в качестве целых чисел передаются в конструктор класса Point, создавая таким образом новый объект
 * <p>
 * Используя метод quarter класса Point, возвращающий в точку вызова номер четверти, которой принадлежит точка, а также
 * конструцию switch, осуществляется подсчет точек в каждой из координатных четвертей. Далее выполняется поиск четверти,
 * в которой содержится наибольшее количество точек
 * <p>
 * С помощью метода lenghtToAsix класса Point, возвращающий в точку вызова наименьшую по модулю координату точки,
 * которая и будет являться кратчайшим расстоянием до оси координат. Поиск ближайшей к осям точки осуществляется только
 * из тех объектов, которые принадлежат самой "многочисленной" четверти
 * <p>
 * Все результаты выводятся в консоль
 * <p>
 * P.S. К сожалению, реализовать работу программы при "неидеальных" тестовых данных не удалось. Но использование
 * конструкции switch предполагает в дальнейшем рассматривать четверти с одинаково большим количеством точек, дабы
 * продолжать отбирать нужную по другим критериям и не терять остальные. Таким образом, на данный момент корректная
 * работа программы согласно заданию будет обеспечена только в том случае, когда лишь в одной четверти количество точек
 * является максимальным
 *
 * @author Горбачева О. Д. 16ИТ18к
 */
public class ManyPoints {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество точек");
        int numberOfPoints = scanner.nextInt();
        Point[] points = new Point[numberOfPoints];
        System.out.println("Введите координаты точек");
        scanner.nextLine();
        String string;
        for (int i = 0; i < numberOfPoints; i++) {
            string = scanner.nextLine();
            String[] strings = string.split(" ");
            points[i] = new Point(Integer.parseInt(strings[0]), Integer.parseInt(strings[1]));
        }

        int[] quarters = new int[4];
        for (Point point : points) {
            switch (point.quarter()) {
                case 1: {
                    quarters[0]++;
                    break;
                }
                case 2: {
                    quarters[1]++;
                    break;
                }
                case 3: {
                    quarters[2]++;
                    break;
                }
                case 4: {
                    quarters[3]++;
                    break;
                }
            }
        }

        int maxNumberOfPoints = quarters[0];
        int index1 = 0;
        for (int i = 1; i < 4; i++) {
            if (quarters[i] > maxNumberOfPoints) {
                maxNumberOfPoints = quarters[i];
                index1 = i;
            }
        }

        int minLenghtToAsix = points[0].lenghtToAsix();
        int index2 = 0;
        for (int i = 0; i < numberOfPoints; i++) {
            if ((points[i].quarter() == (index1 + 1)) && (points[i].lenghtToAsix() < minLenghtToAsix)) {
                minLenghtToAsix = points[i].lenghtToAsix();
                index2 = i;
            }
        }

        System.out.println("Максимальное количество точек в одной четверти составляет " + maxNumberOfPoints);
        System.out.println("Такое количество точек расположено в четверти под номером " + (index1 + 1));
        System.out.println("Наименее удаленной от осей координат в этой четверти является точка " + points[index2]);
        System.out.println("Расстояние от этой точки до ближайшей оси равно " + points[index2].lenghtToAsix());
    }
}